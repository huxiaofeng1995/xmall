package com.bee.member.feign;

import com.bee.member.service.WBAuthoriService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("app-bee-member")
public interface WBAuthoriFeign extends WBAuthoriService {

}
