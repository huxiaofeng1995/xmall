package com.bee.portal.controller;

import com.bee.base.BaseResponse;
import com.bee.member.feign.MemberServiceFeign;
import com.bee.member.output.dto.UserOutDTO;
import com.bee.web.base.BaseWebController;
import com.bee.web.constants.WebConstants;
import com.bee.web.utils.CookieUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**

 * 
 * @description: 首页

 */
@Controller
public class IndexController extends BaseWebController {

	@Autowired
	private MemberServiceFeign memberServiceFeign;

	/**
	 * 跳转到index页面
	 */
	private static final String INDEX_FTL = "index";


	@RequestMapping("/")
	public String index(HttpServletRequest request, HttpServletResponse response, Model model) {
		String token = CookieUtils.getCookieValue(request, WebConstants.LOGIN_TOKEN_COOKIENAME);
		if(StringUtils.isNotEmpty(token)) {
			BaseResponse<UserOutDTO> info = memberServiceFeign.getInfo(token);
			if (isSuccess(info)) {
				UserOutDTO dto = info.getData();
				if (dto != null) {
					String mobile = dto.getMobile();
					// 对手机号码实现脱敏
					String desensMobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
					model.addAttribute("desensMobile", desensMobile);
				}
			}
		}
		return INDEX_FTL;
	}

	/**
	 * 跳转到首页
	 * 
	 * @return
	 */
	@RequestMapping("/index")
	public String indexHtml(HttpServletRequest request, HttpServletResponse response, Model model) {
		return index(request,response,model);
	}
}
