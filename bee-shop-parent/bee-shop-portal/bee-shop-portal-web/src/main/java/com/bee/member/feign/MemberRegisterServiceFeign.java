package com.bee.member.feign;

import com.bee.member.service.MemberRegisterService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("app-bee-member")
public interface MemberRegisterServiceFeign extends MemberRegisterService {
}
