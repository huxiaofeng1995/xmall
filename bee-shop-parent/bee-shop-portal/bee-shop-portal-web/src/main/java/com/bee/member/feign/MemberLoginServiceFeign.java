package com.bee.member.feign;

import com.bee.member.service.MemberLoginService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("app-bee-member")
public interface MemberLoginServiceFeign extends MemberLoginService {
}
