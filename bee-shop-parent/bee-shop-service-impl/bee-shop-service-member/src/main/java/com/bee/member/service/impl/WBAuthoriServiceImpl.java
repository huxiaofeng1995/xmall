package com.bee.member.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bee.base.BaseApiService;
import com.bee.base.BaseResponse;
import com.bee.constants.Constants;
import com.bee.core.token.GenerateToken;
import com.bee.member.mapper.UserMapper;
import com.bee.member.mapper.entity.UserDo;
import com.bee.member.service.WBAuthoriService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WBAuthoriServiceImpl extends BaseApiService<JSONObject> implements WBAuthoriService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private GenerateToken generateToken;

	@Override
	public BaseResponse<JSONObject> findByOpenId(String wbOpenId) {
		// 1.根据wbOpenId查询用户信息
		if (StringUtils.isEmpty(wbOpenId)) {
			return setResultError("wbOpenId不能为空!");
		}
		// 2.如果没有查询到 直接返回状态码203
		UserDo userDo = userMapper.findByOpenId(wbOpenId);
		if (userDo == null) {
			return setResultError(Constants.HTTP_RES_CODE_NOTUSER_203, "根据wbOpenId没有查询到用户信息");
		}
		// 3.如果能够查询到用户信息的话 返回对应用户信息token
		String keyPrefix = Constants.MEMBER_TOKEN_KEYPREFIX + "WB_LOGIN";
		Long userId = userDo.getUserId();
		String userToken = generateToken.createToken(keyPrefix, userId + "");
		JSONObject data = new JSONObject();
		data.put("token", userToken);
		return setResultSuccess(data);
	}

}
