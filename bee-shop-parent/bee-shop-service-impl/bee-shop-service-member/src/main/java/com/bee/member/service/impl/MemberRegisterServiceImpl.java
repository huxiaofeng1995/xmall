package com.bee.member.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bee.base.BaseApiService;
import com.bee.base.BaseResponse;
import com.bee.constants.Constants;
import com.bee.core.bean.MeiteBeanUtils;
import com.bee.core.utils.MD5Util;
import com.bee.member.feign.VerificaCodeServiceFeign;
import com.bee.member.input.dto.UserInpDTO;
import com.bee.member.mapper.UserMapper;
import com.bee.member.mapper.entity.UserDo;
import com.bee.member.service.MemberRegisterService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberRegisterServiceImpl extends BaseApiService<JSONObject> implements MemberRegisterService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private VerificaCodeServiceFeign verificaCodeServiceFeign;


    @Override
    public BaseResponse<JSONObject> register(@RequestBody UserInpDTO userEntity, String registCode) {
        //1.参数验证
        String userName = userEntity.getUserName();
//        if (StringUtils.isEmpty(userName)) {
//            return setResultError("用户名称不能为空!");
//        }
        String mobile = userEntity.getMobile();
        if (StringUtils.isEmpty(mobile)) {
            return setResultError("手机号码不能为空!");
        }
        String password = userEntity.getPassword();
        if (StringUtils.isEmpty(password)) {
            return setResultError("密码不能为空!");
        }
        //2.密码加密
        userEntity.setPassword(MD5Util.MD5(password));
        //3.检查验证码是否有效
        BaseResponse<JSONObject> resultVerificaWeixinCode = verificaCodeServiceFeign.verificaWeixinCode(mobile,
                registCode);
        if (!resultVerificaWeixinCode.getCode().equals(Constants.HTTP_RES_CODE_200)) {
            return setResultError(resultVerificaWeixinCode.getMsg());
        }
        UserDo userDo = MeiteBeanUtils.dtoToDo(userEntity,UserDo.class);
        int registerResult = userMapper.register(userDo);
        return registerResult > 0 ? setResultSuccess("注册成功") : setResultSuccess("注册失败");
    }
}
