package com.bee.member.feign;

import com.bee.weixin.service.VerificaCodeService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("app-bee-weixin")
public interface VerificaCodeServiceFeign extends VerificaCodeService {
}
