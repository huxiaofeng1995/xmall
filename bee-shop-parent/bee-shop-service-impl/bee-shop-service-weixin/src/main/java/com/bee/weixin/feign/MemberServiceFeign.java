package com.bee.weixin.feign;

import com.bee.member.service.MemberService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("app-bee-member")
public interface MemberServiceFeign extends MemberService {

}
