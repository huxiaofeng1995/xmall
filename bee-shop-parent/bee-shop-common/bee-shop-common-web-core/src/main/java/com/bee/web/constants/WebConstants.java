package com.bee.web.constants;

public interface WebConstants {

	String LOGIN_TOKEN_COOKIENAME = "login.pc.token";
	String LOGIN_QQ_OPENID = "qq_openid";
	String LOGIN_WB_OPENID = "wb_openid";
}
