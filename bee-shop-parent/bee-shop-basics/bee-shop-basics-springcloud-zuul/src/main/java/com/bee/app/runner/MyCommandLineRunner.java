package com.bee.app.runner;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigChangeListener;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class MyCommandLineRunner implements CommandLineRunner {//在程序启动后执行，只执行一次

    @ApolloConfig
    private Config config;

    @Override
    public void run(String... args) throws Exception {
        //监听分布式配置中心配置修改事件
        config.addChangeListener(new ConfigChangeListener() {

            @Override
            public void onChange(ConfigChangeEvent changeEvent) {
                //log.debug("####分布式配置中心监听#####" + changeEvent.changedKeys().toString());
                System.out.println(changeEvent.changedKeys().toString());
            }
        });
    }

}

