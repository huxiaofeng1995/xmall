package com.bee.member.service;

import com.alibaba.fastjson.JSONObject;
import com.bee.base.BaseResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 
 * @description: 微博用户授权接口
 */
public interface WBAuthoriService {
	/**
	 * 根据 openid查询是否已经绑定,如果已经绑定，则直接实现自动登陆
	 * 
	 * @param wbOpenId
	 * @return
	 */
	@RequestMapping("/findByOpenId")
	BaseResponse<JSONObject> findByOpenId(@RequestParam("wbOpenId") String wbOpenId);

}
