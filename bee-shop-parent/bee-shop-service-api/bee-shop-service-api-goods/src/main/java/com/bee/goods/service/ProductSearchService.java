package com.bee.goods.service;

import com.bee.base.BaseResponse;
import com.bee.goods.output.dto.ProductDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @description: 商品搜索服务接口
 */
public interface ProductSearchService {

	@GetMapping("/search")
	public BaseResponse<List<ProductDto>> search(@RequestParam("name")String name);

}
